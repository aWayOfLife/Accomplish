package com.example.sahak.accomplish;

import android.arch.lifecycle.LifecycleActivity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends LifecycleActivity {

    private CategoryViewModel categoryViewModel;
    private CategoryRecyclerViewAdapter categoryRecyclerViewAdapter;
    private RecyclerView categoryRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        categoryRecyclerView = (RecyclerView) findViewById(R.id.category_recycler_view);
        categoryRecyclerViewAdapter = new CategoryRecyclerViewAdapter(new ArrayList<CategoryModel>(),width);
        categoryRecyclerView.setLayoutManager(new GridLayoutManager(this,2,LinearLayoutManager.VERTICAL,
                false));

        categoryRecyclerView.setAdapter(categoryRecyclerViewAdapter);

        categoryViewModel = ViewModelProviders.of(this).get(CategoryViewModel.class);

        categoryViewModel.getCategoryAndCountList().observe(MainActivity.this, new Observer<List<CategoryModel>>() {
            @Override
            public void onChanged(@Nullable List<CategoryModel> categoryAndCount) {
                categoryRecyclerViewAdapter.addItems(categoryAndCount);
            }
        });


        FloatingActionButton fab = findViewById(R.id.add_category);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    categoryViewModel.addItem(new CategoryModel(
                            "Work \nProjects",
                            (long) 0
                    ));


            }
        });
    }


}
