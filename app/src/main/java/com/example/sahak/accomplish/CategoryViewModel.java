package com.example.sahak.accomplish;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import java.util.List;

/**
 * Created by sahak on 18-Sep-17.
 */

public class CategoryViewModel extends AndroidViewModel{

    private final LiveData<List<CategoryModel>> categoryAndCountList;

    private CategoryDatabase categoryDatabase;

    public CategoryViewModel(Application application)
    {
        super(application);
        categoryDatabase = CategoryDatabase.getDatabase(this.getApplication());
        categoryAndCountList = categoryDatabase.categoryAndCountModel().getAllCategoryItems();

    }

    public LiveData<List<CategoryModel>> getCategoryAndCountList()
    {
        return categoryAndCountList;
    }

    public void deleteItem(CategoryModel categoryModel)
    {
        new deleteAsyncTask(categoryDatabase).execute(categoryModel);
    }

    public void addItem(final CategoryModel categoryModel) {
        new addAsyncTask(categoryDatabase).execute(categoryModel);
    }

    private static class deleteAsyncTask extends AsyncTask<CategoryModel, Void, Void> {

        private CategoryDatabase db;

        deleteAsyncTask(CategoryDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final CategoryModel... params) {
            db.categoryAndCountModel().deleteCategory(params[0]);
            return null;
        }

    }

    private static class addAsyncTask extends AsyncTask<CategoryModel, Void, Void> {

        private CategoryDatabase db;

        addAsyncTask(CategoryDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final CategoryModel... params) {
            db.categoryAndCountModel().addCategory(params[0]);
            return null;
        }

    }
}
