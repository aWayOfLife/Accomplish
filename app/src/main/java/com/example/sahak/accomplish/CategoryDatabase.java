package com.example.sahak.accomplish;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by sahak on 18-Sep-17.
 */
@Database(entities = {CategoryModel.class}, version = 3)
public abstract class CategoryDatabase extends RoomDatabase {

    private static CategoryDatabase INSTANCE;

    public static CategoryDatabase getDatabase(Context context)
    {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), CategoryDatabase.class, "categories_db")
                            .build();
        }
        return INSTANCE;
    }

    public abstract CategoryModelDao categoryAndCountModel();
}
