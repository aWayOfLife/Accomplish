package com.example.sahak.accomplish;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by sahak on 18-Sep-17.
 */
@Entity
public class CategoryModel {

    @PrimaryKey(autoGenerate = true)
    public int id;

    private String categoryName;
    private Long categoryCount;

    public CategoryModel(String categoryName, Long categoryCount) {
        this.categoryName = categoryName;
        this.categoryCount = categoryCount;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Long getCategoryCount() {
        return categoryCount;
    }
}
