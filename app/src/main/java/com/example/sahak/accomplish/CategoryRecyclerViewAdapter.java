package com.example.sahak.accomplish;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sahak on 18-Sep-17.
 */

public class CategoryRecyclerViewAdapter extends RecyclerView.Adapter<CategoryRecyclerViewAdapter.RecyclerViewHolder> {

    private List<CategoryModel> categoryModelList;
    private View.OnLongClickListener longClickListener;
    private int width;

    public CategoryRecyclerViewAdapter(List<CategoryModel> categoryModelList,int width) {
        this.categoryModelList = categoryModelList;
        this.width = width;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        CategoryModel categoryModel = categoryModelList.get(position);
        holder.categoryName.setText(categoryModel.getCategoryName());
        holder.categoryCount.setText(categoryModel.getCategoryCount()+" Tasks");

        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        // Set the height by params
        params.height=(int)Math.ceil(width/2.4);
        params.width=width/2;
        // set height of RecyclerView
        holder.itemView.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }

    public void addItems(List<CategoryModel> categoryModelList) {
        this.categoryModelList = categoryModelList;
        notifyDataSetChanged();
    }


    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView categoryName;
        private TextView categoryCount;

        RecyclerViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.category_name);
            categoryCount = (TextView) view.findViewById(R.id.category_count);
        }
    }
}
